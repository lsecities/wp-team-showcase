jQuery(function($) {
    // https://tc39.github.io/ecma262/#sec-array.prototype.includes
    // from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function (searchElement, fromIndex) {
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                var len = o.length >>> 0;

                if (len === 0) {
                    return false;
                }

                var n = fromIndex | 0;

                var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                while (k < len) {
                    if (o[k] === searchElement) {
                        return true;
                    }
                    k++;
                }

                return false;
            }
        });
    }

    /**
     * If page contains a team showcase element (generated via WP shortcode
     * from this plugin), then add an ".lcts-page" class to the body. This is
     * used to set overflow: visible on .section elements only on pages
     * where this shortcode is used. This, in turn, is needed for the sticky
     * positioning of the team showcase filters area to work correctly. */
    if($('.lcts-container').length) {
        $('body').addClass('lcts-page');
    }

    const filter_facets = ['cohort', 'sector', 'city', 'country'];

    $('.filter-facet').on('change', function() {
        let filter_values = filter_facets.map(function(facet) {
            return $('#filter-by-' + facet).find(':selected').attr('name');
        });

        if($('#filter-by-city').find(':selected').attr('name') !== 'default') {
            $('#filter-by-country').prop('disabled', true);
        } else {
            $('#filter-by-country').prop('disabled', false);
        }

        if($('#filter-by-country').find(':selected').attr('name') !== 'default') {
            $('#filter-by-city').prop('disabled', true);
        } else {
            $('#filter-by-city').prop('disabled', false);
        }

        $('.lcts-member').each(function() {
            let el = this;
            let item_filterables = filter_facets.map(function(item, i) {
                /**
                 * To allow to match across multiple, space-separated values
                 * for a facet, first we split the space-separated string
                 * for the member's value of this facet, then we test whether
                 * the requested filter value is present in the array returned
                 * by String.prototype.split().
                 * This only applies to the "sector" facet (one person can
                 * be associated to 0 or more sectors, whereas they can be
                 * associated to only one cohort, city and country).
                 */

                // make sure item_data_for_facet is a string (in case the
                // value is accidentally a number, e.g. if slug is auto-assigned
                // by Pods)
                const item_data_for_facet = '' + $(el).data(item);
                // then split over ' '
                const item_values_for_facet = item_data_for_facet.split(' ');
                // and then test for matching values
                return filter_values[i] === 'default' || item_values_for_facet.includes(filter_values[i]);
            });

            if(item_filterables.includes(false)) {
                $(this).hide();
            } else {
                $('.lcts-no-results').hide();
                $(this).show();
            }
        });

        if($('.lcts-member:visible').length === 0) {
            $('.lcts-no-results').show();
        }
    });

    $('.lcts-clear-filters, .lcts-no-results').on('click', function() {
        filter_facets.forEach(function(item) {
            $('#filter-by-' + item).val($('#filter-by-' + item + ' option:first').val());
        });

        $('.lcts-no-results').hide();
        $('#filter-by-city').prop('disabled', false);
        $('#filter-by-country').prop('disabled', false);
        $('.lcts-member').show();
    });
});