<div class="wmts_member lcts-member" data-slug="<?php echo $person['slug']; ?>" data-cohort="<?php echo $person['cohort_slug']; ?>" data-sector="<?php echo $person['sector_slug']; ?>" data-city="<?php echo $person['city_slug']; ?>" data-country="<?php echo $person['country_slug']; ?>">
    <a data-fancybox data-src="#member__<?php echo $person['slug']; ?>" href="javascript:;">
    <?php $details_mode = FALSE; include "team-member-data.php"; ?>
</a>
</div> <!-- .lcts-member -->
<div id="member__<?php echo $person['slug']; ?>" class="lcts-member-lightbox">
<?php $details_mode = TRUE; include "team-member-data.php"; ?>
    <div class="lcts-member-blurb">
        <?php echo wpautop($person['description'], FALSE); ?>
    </div>
</div>