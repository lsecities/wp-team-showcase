<div class="wmts_container wmts_loaded lcts-container">
    <div class="wmts_members lcts-members">
<?php foreach($team_members as $person) {
    include 'team-member.php';
} ?>
    </div> <!-- .lcts-members -->
    <div class="lcts-no-results"><a href="javascript:;">No results found. Clear search filters?</a></div>
</div> <!-- .lcts-container -->