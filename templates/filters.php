<div class="lcts-filters">
    <h3>Filter by:</h3>
<?php foreach(['cohort', 'sector', 'city', 'country'] as $facet_name) : ?>
    <select class="filter-facet" id="filter-by-<?php echo $facet_name; ?>" name="<?php echo $facet_name; ?>">
        <option name="default" selected><?php echo "select " . $facet_name; ?></option>
<?php foreach($filters[$facet_name] as $f) : ?>
        <option name="<?php echo $f['slug']; ?>"><?php echo $f['title']; ?></option>
<?php endforeach; ?>
    </select>
<?php endforeach; ?>
    <button class="lcts-clear-filters">clear all filters</button>
</div>
