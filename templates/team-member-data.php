<div class="lcts-member-core-data">    
    <div class="<?php if(!$details_mode) : ?>wmts_image_centering <?php endif; ?>lcts-image">
        <img src="<?php echo $person['photo_uri']; ?>" />
    </div>
    <div class="<?php if(!$details_mode) : ?>wmts_text_container <?php endif; ?>lcts-text-container">
        <h2 class="<?php if(!$details_mode) : ?>wmts_name <?php endif; ?>lcts-name">
            <?php echo $person['first_name'] . ' ' . $person['family_name']; ?>
        </h2>
<?php if(!empty($person['role'])) : ?>
        <h3 class="<?php if(!$details_mode) : ?>wmts_job_title <?php endif; ?>lcts-role">
            <?php echo $person['role']; ?>
        </h3>
<?php endif; ?>
<?php if(!empty($person['affiliation'])) : ?>
        <h3 class="<?php if(!$details_mode) : ?>wmts_job_title <?php endif; ?>lcts-affiliation">
            <?php echo $person['affiliation']; ?>
        </h3>
<?php endif; ?>
        <div class="lcts-location">
<?php if(!empty($person['city'])) : ?>
            <span class="lcts-city">
                <?php echo $person['city']; ?><?php if(!empty($person['country'])) { ?>,<?php } ?>
            </span>
<?php endif; ?>
<?php if(!empty($person['country'])) : ?>
            <span class="lcts-country">
                <?php echo $person['country']; ?>
            </span>
<?php endif; ?>
        </div>
        <div class="lcts-cohort">
            Class of <?php echo $person['cohort']; ?>
        </div>
    </div>
</div>