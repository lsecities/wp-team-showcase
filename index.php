<?php
/*
Plugin Name: Team Showcase
Plugin URI:  https://gitlab.com/lsecities/wp-team-showcase/
Description: Team Showcase plugin for WordPress + Pods CMS
Version:     0.1.0
Author:      Andrea Rota, LSE Cities
Author URI:  https://gitlab.com/lsecities/
License:     AGPL3+

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

//namespace LSECities\WP\Plugin\TeamShowcase;

wp_enqueue_style('wmts_css', plugin_dir_url( __FILE__ ) . 'vendor/css/wmts.css');
wp_enqueue_style('lcts_css', plugin_dir_url( __FILE__ ) . 'assets/css/team-showcase.css');
wp_enqueue_script('lcts_js', plugin_dir_url(__FILE__) . 'assets/js/team-showcase.js', ['jquery'], FALSE, TRUE);
wp_enqueue_style('fancybox_css', plugin_dir_url(__FILE__) . 'vendor/fancybox/jquery.fancybox.min.css');
wp_enqueue_script('fancybox_js', plugin_dir_url(__FILE__) . 'vendor/fancybox/jquery.fancybox.min.js', [], FALSE, TRUE);

function theme_showcase_shortcode($attributes = [], $content = NULL) {
    // Pods find parameters for Pods to be used as filters; these all
    // share the same query params (no limit, order by item title)
    $default_pods_find_params = [
        'limit' => -1,
        'orderby' => 'title ASC'
    ];

    $people = pods('person')->find([
        //'where' => 'groups.id IN ("' . $attributes['id'] . '")'
        'limit' => -1,
        'orderby' => 'family_name ASC'
    ]);

    if($people->total() > 0) {
        while($people->fetch()) {
            // Use images at 780x520 size
            // URI is first member of array returned by wp_get_attachment_image_src()
            $photo_uri = wp_get_attachment_image_src($people->field('photo.ID'), [780,520])[0];

            $team_members[] = [
                'slug' => $people->field('slug'),
                'family_name' => $people->field('family_name'),
                'first_name' => $people->field('first_name'),
                'photo_uri' => $photo_uri,
                'description' => $people->field('description'),
                'role' => $people->field('role'),
                'affiliation' => $people->field('affiliation'),
                'city' => $people->field('city.city'),
                'city_slug' => $people->field('city.post_name'),
                'country' => $people->field('city.country.country'),
                'country_slug' => $people->field('city.country.post_name'),
                'cohort' => $people->field('cohort.cohort'),
                'cohort_slug' => $people->field('cohort.post_name'),
                'sector' => implode(', ', $people->field('sector.sector')),
                'sector_slug' => implode(' ', $people->field('sector.post_name'))
            ];
        }
    }

    if($attributes['show_filters']) {
        // Empty array for filters. This will be populated with data extracted
        // from filter Pods.
        $filters = [];

        foreach(['cohort', 'sector', 'city', 'country'] as $facet_name) {
            $filter = pods($facet_name)->find($default_pods_find_params);
            while($filter->fetch()) {
                $filters[$facet_name][] = [
                    'slug' => $filter->field('slug'),
                    'title' => $filter->field('title')
                ];
            }
        }
    }

    // Capture output from included files
    ob_start();

    if($attributes['show_filters'] and $people->total() > 0) {
        include 'templates/filters.php';
    }

    if($people->total() > 0) {
        include 'templates/team-showcase.php';
    }

    // Return captured output
    return ob_get_clean();
}

add_shortcode('lcts', 'theme_showcase_shortcode');