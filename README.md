# Team Showcase plugin for LSE Cities

## Requirements

* list of people, grid display:
  * photo (top)
  * metadata (beneath):
    * name
    * role/affiliation (if configured)
* on click/tap, lightbox with:
  * photo (left half)
  * metadata (right half):
    * name
    * role/affiliation
    * city
    * country
    * industry sector
    * cohort

Responsive display; Isotope for transitions.

### Filter

* list must be filterable by
  * year/cohort
  * industry sector
  * city
  * country

Each filter facet should support only one concurrent selection.
City and country are mutually exclusive filters: if a visitor selects a city,
the country filter is cleared, and vice versa.

### Possible future requirements

Map showing geo distribution of people within the current filter set.

Configure shortcode with preset filters (may be tricky if also wanting to
limit which filters or filter values can be applied, e.g. "only show 2017/18
cohort", or "only show people from sectors X, Y and Z").

## Development

### Data

Pods plugin

* People
  * Family name
  * First name
  * Role
  * Affiliation
  * City (link to Cities pod)
  * Country (link to Countries pod)
  * Industry sector (link to Industry sectors pod)
  * Cohort (link to Cohorts pod)
* Groups (link to People pod)
* Industry sectors
* Cities
  * Name
  * Latitude
  * Longitude
  * Country (link to Countries pod)
* Countries
  * Name
  * Cities (link to Cities pod)
* Cohorts

### Shortcode

`[team-showcase group=<group_id>]`

### HTML

Pods metadata -> PHP templates -> HTML for filter UX.

Pods data -> PHP templates -> HTML for content.

### JavaScript

Isotope for filters and transitions.

## License

(C) Copyright LSE Cities 2017

This plugin is distributed under the GNU Affero GPL license, version 3 or
later.